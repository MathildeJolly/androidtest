package com.example.firstapp

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.gson.Gson
import kotlinx.android.synthetic.main.main_fragement.*

class MainFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.main_fragement, container, false);
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val layoutManager = LinearLayoutManager(this.activity)
        layoutManager.orientation = LinearLayoutManager.VERTICAL
        recyclerView.layoutManager = layoutManager

        val queue = Volley.newRequestQueue(this.activity)
        val url = "https://my-json-server.typicode.com/bgdom/cours-android/games"

        val stringRequest = StringRequest(
            Request.Method.GET, url,
            Response.Listener<String> { response ->
                val obj = Gson().fromJson(response, Array<GameClass>::class.java);
                recyclerView.adapter = TextAdapter(object:GameAdapterInterface{
                    override fun open(game: GameClass) {
                        this@MainFragment.fragmentManager!!
                            .beginTransaction()
                            .addToBackStack(null)
                            .replace(R.id.fragment, DetailFragment(game)).commit();
                    }

                    override val games: Array<GameClass> = obj;
                });



            },
            Response.ErrorListener {
                Log.d("response", it.toString());
            })

        queue.add(stringRequest)
    }
}
