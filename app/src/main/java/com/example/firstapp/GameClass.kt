package com.example.firstapp

data class GameClass (
    val id: Int,
    val name: String,
    val description: String,
    val link: String,
    val img: String
)