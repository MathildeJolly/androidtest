package com.example.firstapp

interface GameAdapterInterface {
    val games: Array<GameClass>
    fun open (game: GameClass)
}
